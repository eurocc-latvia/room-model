#!/bin/bash

### These are the instructions for setting up the environment in HPC,
### these commands should be run after every login
module load openfoam/openfoam-6
source /software/openfoam/OpenFOAM-6/etc/bashrc
module load singularity
module load anaconda3
module load paraview
echo "Check if OpenFOAM-6, openmpi, pvpython and singularity are loaded:"
which icoFoam
which singularity
which pvpython
echo "Please load the required programs manually or ask your administrator if some of the listed programs is not found"

echo ""
conda info --envs
echo "Please input the name of conda environment that should be used and press Enter"
read env_name
echo Activating $env_name
conda activate $env_name

module load openmpi4/4.1.1
which mpirun
echo "Done!"

### If there are no conda environments except base, then create a new environment by running this command: 
# conda create --name NAME_OF_NEW_ENV

### The required python modules are listed below
### They should be installed after the activation of conda environment, but before running everything else:
# conda install pip
# pip install   numpy numpy-stl pandas matplotlib openpyscad

### before executing the next .sh scripts, the solver should be compiled:
# cd scalarTransportTurbulentFoam
# wclean && wmake
### check if solver compilation is successfull (the path to the solver executable should be returned):
# which scalarTransportTurbulentFoam
