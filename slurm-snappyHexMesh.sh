#!/bin/bash
#SBATCH --job-name=EuroCC-demo_meshing   # Job name
#SBATCH --ntasks=16                      # Number of tasks
#SBATCH --partition=regular              # Partition name
#SBATCH --mem-per-cpu=2G                 # Memory per cpu-core
#SBATCH --time=694:00:00                 # Time limit hrs:min:sec

echo "Current dir = "; pwd; hostname; date
decomposePar -force
mpirun -np 16 snappyHexMesh -overwrite -parallel >./log-snappyHexMesh
reconstructParMesh -constant

topoSet -dict system/topoSetDict_REF_walls
refineMesh -overwrite -dict system/refineMeshDictX
refineMesh -overwrite -dict system/refineMeshDictY
refineMesh -overwrite -dict system/refineMeshDictZ

### draw mesh
pvpython draw-mesh.py

