### postProcess -func singleGraph
import matplotlib.pyplot as plt
import numpy as np
import os

for timeDir in os.listdir('postProcessing/singleGraph'):
    data = np.loadtxt('postProcessing/singleGraph/'+timeDir+'/line_T.xy')
    plt.figure(figsize=(6,5))
    plt.plot(data[:,0], data[:,1]-273, label='centre')
    ax = plt.gca()
    ax.set_xlabel('z, m')
    ax.set_ylabel(r'T, $^o$C')
    ax.set_ylim([15,23])
    # plt.legend()
    plt.grid()
    plt.savefig('line-T-{0}.png'.format(timeDir))
    plt.clf()
