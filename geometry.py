import openpyscad as ops
import numpy as np
from parameters import *
from headers import *


eps = 0.0001 # offset/precision for geometry creation
# checking topology of the room
if z_cond_position<(z_door+eps):
    print('Warning: [z_cond_position] should be greater than [z_door]')
if (y_win_position+y_win)>y_room or (z_win_position+z_win)>z_room:
    print('Warning: window size is too large for given window position and room size')
if (y_door_position+y_door)>y_room or z_door>z_room:
    print('Warning: door size is too large for given door position and room size')
if (y_cond_position+y_cond)>y_room or (z_cond_position+z_cond)>z_room:
    print('Warning: conditioner size is too large for given conditioner position and room size')


# Creating room geometry and writing instructions for OpenSCAD
print('Creating room geometry and writing instructions for OpenSCAD...')
c1 = ops.Cube([x_room, y_room, z_room])
c2 = ops.Cube([x_door+eps, y_door, z_door]).translate([-x_door, y_door_position, 0])
c3 = ops.Cube([x_win+eps, y_win, z_win]).translate([x_room-eps, y_win_position, z_win_position])
allShape = c1 + c2 + c3 # room + door + window
c4 = ops.Cube([x_cond+eps, y_cond, z_cond])
c5 = ops.Cube([1, 1, 1]).rotate([0, 45, 0])
c5 = c5.translate([0, 0, -condHoriz])
c4 = c4 - c5
c4 = c4.translate([-eps, y_cond_position, z_cond_position])
allShape = allShape - c4
allShape.write("geometry.scad")

print('Moving a human figure and writing instructions for OpenSCAD...')
stringToInsert = 'translate([{0},{1},0-0.01]) \n'.format(x_man_postion, y_man_postion)
replaceIn('geometry-man.scad', 'translate(', stringToInsert)


# Changing blockmeshDict: size of outer boundaries, number of cells, mesh layers
print('Changing system/blockmeshDict...')
fOut = open('system/blockMeshDict', 'w')
fOut.write(blockMeshDictHeader)
fOut.write('\t({0}   {1}   {2})\n'.format(-x_door-eps,      -eps,       -eps))
fOut.write('\t({0}   {1}   {2})\n'.format(x_room+x_win+eps, -eps,       -eps))
fOut.write('\t({0}   {1}   {2})\n'.format(x_room+x_win+eps, y_room+eps, -eps))
fOut.write('\t({0}   {1}   {2})\n'.format(-x_door-eps,      y_room+eps, -eps))
fOut.write('\t({0}   {1}   {2})\n'.format(-x_door-eps,      -eps,       z_room+eps))
fOut.write('\t({0}   {1}   {2})\n'.format(x_room+x_win+eps, -eps,       z_room+eps))
fOut.write('\t({0}   {1}   {2})\n'.format(x_room+x_win+eps, y_room+eps, z_room+eps))
fOut.write('\t({0}   {1}   {2})\n'.format(-x_door-eps,      y_room+eps, z_room+eps))
fOut.write(""");

blocks
(
    hex (0 1 2 3 4 5 6 7) """)
fOut.write('\t({0}   {1}   {2})\n'.format(int(n_x*(x_room+x_door+x_win)), int(n_y*y_room), int(n_z*z_room)))
fOut.write("""    simpleGrading 
    (
        (""")
cellsNearWallX = n_x*layer*(1+rate/5)
cellsNearWallY = n_y*layer*(1+rate/5)
cellsNearWallZ = n_z*layer*(1+rate/5)
fOut.write('\t\t\t({0}  {1}  {2})\n'.format(layer, cellsNearWallX, 1))
fOut.write('\t\t\t({0}  {1}  {2})\n'.format((x_room/2-layer), n_x*x_room/2-cellsNearWallX, rate))
fOut.write('\t\t\t({0}  {1}  {2})\n'.format((x_room/2-layer), n_x*x_room/2-cellsNearWallX, 1/rate))
fOut.write('\t\t\t({0}  {1}  {2})\n'.format(layer, cellsNearWallX, 1))
fOut.write('\t\t) // x-direction\n')
fOut.write('\t\t(\n')
fOut.write('\t\t\t({0}  {1}  {2})\n'.format(layer, cellsNearWallY, 1))
fOut.write('\t\t\t({0}  {1}  {2})\n'.format((y_room/2-layer), n_y*y_room/2-cellsNearWallY, rate))
fOut.write('\t\t\t({0}  {1}  {2})\n'.format((y_room/2-layer), n_y*y_room/2-cellsNearWallY, 1/rate))
fOut.write('\t\t\t({0}  {1}  {2})\n'.format(layer, cellsNearWallY, 1))
fOut.write('\t\t) // y-direction\n')
fOut.write('\t\t(\n')
fOut.write('\t\t\t({0}  {1}  {2})\n'.format(layer, cellsNearWallZ, 1))
fOut.write('\t\t\t({0}  {1}  {2})\n'.format((z_room/2-layer), n_z*z_room/2-cellsNearWallZ, rate))
fOut.write('\t\t\t({0}  {1}  {2})\n'.format((z_room/2-layer), n_z*z_room/2-cellsNearWallZ, 1/rate))
fOut.write('\t\t\t({0}  {1}  {2})\n'.format(layer, cellsNearWallZ, 1))
fOut.write('\t\t) // z-direction\n')
fOut.write(blockMeshDictFooter)
fOut.close()


# Writing coordinates of patches in system/topoSetDict_*
print('Writing coordinates of patches...')
eps2 = 0.0003 # for the selection of surfaces
# ceiling
stringToInsert = '            box ({0} {1} {2}) ({3} {4} {5});\n'.format(-x_door-eps2, -eps2, z_room-eps2,   x_room+x_win+eps2, y_room+eps2, z_room+eps2)
replaceIn('system/topoSetDict_ceiling', 'box (', stringToInsert)
# floor
stringToInsert = '            box ({0} {1} {2}) ({3} {4} {5});\n'.format(-x_door-eps2, -eps2, -eps,   x_room+x_win+eps2, y_room+eps2, eps)
replaceIn('system/topoSetDict_floor', 'box (', stringToInsert)
# door
stringToInsert = '            box ({0} {1} {2}) ({3} {4} {5});\n'.format(-x_door-eps2, y_door_position-eps2, -eps2,   -x_door+eps2, y_door_position+y_door+eps2, z_door+eps2)
replaceIn('system/topoSetDict_door', 'box (', stringToInsert)
# window
stringToInsert = '            box ({0} {1} {2}) ({3} {4} {5});\n'.format(x_room+x_door-eps2, y_win_position-eps2, z_win_position-eps2,   x_room+x_door+eps2, y_win_position+y_win+eps2, z_win_position+z_win+eps2)
replaceIn('system/topoSetDict_window', 'box (', stringToInsert)
# in
stringToInsert = '            box ({0} {1} {2}) ({3} {4} {5});\n'.format(condHoriz-eps2, y_cond_position+eps2, z_cond_position-eps2,   x_cond+eps2, y_cond_position+y_cond-eps2, z_cond_position+(x_cond-condHoriz)+eps2)
replaceIn('system/topoSetDict_in', 'box (', stringToInsert)
# out
stringToInsert = '		    p1          ({0} {1} {2});\n'.format(x_room-eps2, y_outlet_position, z_outlet_position)
replaceIn('system/topoSetDict_out', 'p1 ', stringToInsert)
stringToInsert = '		    p2          ({0} {1} {2});\n'.format(x_room+eps2, y_outlet_position, z_outlet_position)
replaceIn('system/topoSetDict_out', 'p2 ', stringToInsert)
stringToInsert = '            radius      {0};\n'.format(r_outlet)
replaceIn('system/topoSetDict_out', 'radius ', stringToInsert)
# nose of a human
x_nose = x_man_postion - 0.105
z_nose = 1.4235
stringToInsert = '		    p1          ({0} {1} {2});\n'.format(x_nose, y_man_postion-0.1, z_nose)
replaceIn('system/topoSetDict_nose', 'p1 ', stringToInsert)
stringToInsert = '		    p2          ({0} {1} {2});\n'.format(x_nose, y_man_postion, z_nose)
replaceIn('system/topoSetDict_nose', 'p2 ', stringToInsert)



# changing regions that will be refined in system/snappyHexMeshDict
print('Modifying refinement regions in system/snappyHexMeshDict...')
dist = r_cyl/2
inletMidX = (x_cond+condHoriz)/2
inletMidZ = z_cond_position + (x_cond-condHoriz)/2
file = 'system/snappyHexMeshDict'
# large cylinder
stringToInsert = '		point1 ({0} {1} {2}); //p1-c1\n'.format(x_cond, y_cond_position-x_cond/4, z_cond_position-dist)
replaceIn(file, 'p1-c1', stringToInsert)
stringToInsert = '		point2 ({0} {1} {2}); //p2-c1\n'.format(x_cond, y_cond_position+y_cond+x_cond/4, z_cond_position-dist)
replaceIn(file, 'p2-c1', stringToInsert)
stringToInsert = '		radius {0}; //r-c1\n'.format(r_cyl)
replaceIn(file, 'r-c1', stringToInsert)
# smaller cylinder
stringToInsert = '		point1 ({0} {1} {2}); //p1-c2\n'.format(inletMidX, y_cond_position, inletMidZ)
replaceIn(file, 'p1-c2', stringToInsert)
stringToInsert = '		point2 ({0} {1} {2}); //p2-c2\n'.format(inletMidX, y_cond_position+y_cond, inletMidZ)
replaceIn(file, 'p2-c2', stringToInsert)
stringToInsert = '		radius {0}; //r-c2\n'.format(r_cyl2)
replaceIn(file, 'r-c2', stringToInsert)
# cylinder around the human's nose
stringToInsert = '		point1 ({0} {1} {2}); //p1-c3\n'.format(x_nose, y_man_postion-0.2, z_nose)
replaceIn(file, 'p1-c3', stringToInsert)
stringToInsert = '		point2 ({0} {1} {2}); //p2-c3\n'.format(x_nose, y_man_postion, z_nose)
replaceIn(file, 'p2-c3', stringToInsert)
stringToInsert = '		radius {0}; //r-c3\n'.format(r_cyl_man)
replaceIn(file, 'r-c3', stringToInsert)



# changing layers that should be refined with refineMesh
# the thickness (layer2) is defined in parameters.py
print('Modifying refinement regions in system/topoSetDict_REF_walls...')
file = 'system/topoSetDict_REF_walls'
# walls with Z normal
stringToInsert = '            box ({0} {1} {2}) ({3} {4} {5}); //layer_Z_left\n'.format(-x_door-eps, -eps, -eps, x_man_postion-0.4, y_room+eps, layer2)
replaceIn(file, 'layer_Z_left', stringToInsert)
stringToInsert = '            box ({0} {1} {2}) ({3} {4} {5}); //layer_Z_right\n'.format(x_man_postion+0.4, -eps, -eps, x_room+x_win+eps, y_room+eps, layer2)
replaceIn(file, 'layer_Z_right', stringToInsert)
stringToInsert = '            box ({0} {1} {2}) ({3} {4} {5}); //layer_Z_front\n'.format(-x_door-eps, -eps, -eps, x_room+x_win+eps, y_man_postion-0.3, layer2)
replaceIn(file, 'layer_Z_front', stringToInsert)
stringToInsert = '            box ({0} {1} {2}) ({3} {4} {5}); //layer_Z_back\n'.format(-x_door-eps, y_man_postion+0.3, -eps, x_room+x_win+eps, y_room+eps, layer2)
replaceIn(file, 'layer_Z_back', stringToInsert)
stringToInsert = '            box ({0} {1} {2}) ({3} {4} {5}); //layer_Z_ceiling\n'.format(-x_door-eps, -eps, z_room-layer2, x_room+x_win+eps, y_room+eps, z_room+eps)
replaceIn(file, 'layer_Z_ceiling', stringToInsert)
# walls with Y normal
stringToInsert = '            box ({0} {1} {2}) ({3} {4} {5}); //layer_Y_front\n'.format(-x_door-eps, -eps, -eps, x_room+x_win+eps, layer2, z_room+eps)
replaceIn(file, 'layer_Y_front', stringToInsert)
stringToInsert = '            box ({0} {1} {2}) ({3} {4} {5}); //layer_Y_back\n'.format(-x_door-eps, y_room-layer2, -eps, x_room+x_win+eps, y_room+eps, z_room+eps)
replaceIn(file, 'layer_Y_back', stringToInsert)
# walls with X normal
stringToInsert = '            box ({0} {1} {2}) ({3} {4} {5}); //layer_X_left\n'.format(-x_door-eps, -eps, -eps, layer2, y_room+eps, z_room+eps)
replaceIn(file, 'layer_X_left', stringToInsert)
stringToInsert = '            box ({0} {1} {2}) ({3} {4} {5}); //layer_X_right\n'.format(x_room-layer2, -eps, -eps, x_room+x_win+eps, y_room+eps, z_room+eps)
replaceIn(file, 'layer_X_right', stringToInsert)





# changing coordinates of the probe points
# by default, probes are put at x=0 near the center of the door
#    and at x=x_room near the center of the window
file = 'system/controlDict'
stringToInsert = '            ( {0} {1} {2} ) //pr_door\n'.format(eps, y_door_position+y_door/2, z_door/2)
replaceIn(file, 'pr_door', stringToInsert)
stringToInsert = '            ( {0} {1} {2} ) //pr_window\n'.format(x_room-eps, y_win_position+y_win/2, z_win_position+z_win/2)
replaceIn(file, 'pr_win', stringToInsert)
file = 'calculate-C/system/controlDict'
stringToInsert = '            ( {0} {1} {2} ) //pr_door\n'.format(eps, y_door_position+y_door/2, z_door/2)
replaceIn(file, 'pr_door', stringToInsert)
stringToInsert = '            ( {0} {1} {2} ) //pr_window\n'.format(x_room-eps, y_win_position+y_win/2, z_win_position+z_win/2)
replaceIn(file, 'pr_win', stringToInsert)




# changing processor number for mesh generation
# using multiple processors
file = 'system/decomposeParDict'
stringToInsert = 'numberOfSubdomains {0};\n'.format(n_proc)
replaceIn(file, 'numberOfSubdomains', stringToInsert)
file = 'slurm-snappyHexMesh.sh'
stringToInsert = '#SBATCH --ntasks={0}               # Number of tasks\n'.format(n_proc)
replaceIn(file, 'ntasks', stringToInsert)
stringToInsert = 'mpirun -np {0} snappyHexMesh -overwrite -parallel >./log-snappyHexMesh\n'.format(n_proc)
replaceIn(file, 'snappyHexMesh', stringToInsert)
