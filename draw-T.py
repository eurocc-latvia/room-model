#### import the simple module from the paraview
from paraview.simple import *
from parameters import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'OpenFOAMReader'
afoam = OpenFOAMReader(FileName='./a.foam')

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
renderView1.ViewSize = [1170, 796]

# get layout
layout1 = GetLayout()

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Properties modified on afoam
afoam.CellArrays = ['T']



# create a new 'Slice'
slice1 = Slice(Input=afoam)

# show data in view
slice1Display = Show(slice1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
slice1Display.Representation = 'Surface'

# hide data in view
# Hide(afoam, renderView1)
# get display properties
afoamDisplay = GetDisplayProperties(afoam, view=renderView1)
# Properties modified on afoamDisplay
afoamDisplay.Opacity = 0.2

# Properties modified on slice1.SliceType
slice1.SliceType.Origin = [x_room/2+0.001, y_room/2, z_room/2] # originYZ
slice1.SliceType.Normal = [1.0, 0.0, 0.0]




# set active source
SetActiveSource(slice1)

# set scalar coloring
ColorBy(slice1Display, ('POINTS', 'T'))

# rescale color and/or opacity maps used to include current data range
slice1Display.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
slice1Display.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'T'
uLUT = GetColorTransferFunction('T')

# get opacity transfer function/opacity map for 'T'
uPWF = GetOpacityTransferFunction('T')

# Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
# uLUT.ApplyPreset('Viridis (matplotlib)', True)

# Properties modified on uLUT
uLUT.NumberOfTableValues = 24

# get color legend/bar for uLUT in view renderView1
uLUTColorBar = GetScalarBar(uLUT, renderView1)
# Properties modified on uLUTColorBar
uLUTColorBar.RangeLabelFormat = '%-#6.4g'
uLUTColorBar.Title = 'T,'
uLUTColorBar.ComponentTitle = 'K'




# current camera placement for renderView1
renderView1.CameraPosition = [-y_room*4, y_room/2, z_room/2] # camposYZ
renderView1.CameraFocalPoint = [y_room/4, y_room/2, z_room/2] # camfocYZ
renderView1.CameraViewUp = [0.0, 0.0, 1.0]
renderView1.CameraParallelScale = 1

# reset view to fit data
renderView1.ResetCamera()
renderView1.Update()

# save screenshot
SaveScreenshot('./T-YZ.png', renderView1, ImageResolution=[1170, 796])









# Properties modified on slice1.SliceType
slice1.SliceType.Normal = [0.0, 0.0, 1.0]
slice1.SliceType.Origin = [x_room/2, y_room/2, z_room/2+0.001] # originXY

# create a new 'Surface Vectors'
# surfaceVectors1 = SurfaceVectors(Input=slice1)
# set active source
# SetActiveSource(surfaceVectors1)
# surfaceVectors1Display = Show(surfaceVectors1, renderView1, 'GeometryRepresentation')
# set scalar coloring
# ColorBy(surfaceVectors1Display, ('POINTS', 'U', 'Magnitude'))
# get color transfer function/color map for 'U'
# uLUT = GetColorTransferFunction('U')
# rescale color and/or opacity maps used to include current data range
uLUT.RescaleTransferFunctionToDataRange()

# current camera placement for renderView1
renderView1.CameraPosition = [x_room/2, y_room/2, z_room*5.2] # camposXY
renderView1.CameraFocalPoint = [x_room/2, y_room/2, 3*z_room/4] # camfocXY
renderView1.CameraViewUp = [0.0, 1.0, 0.0]
renderView1.CameraParallelScale = 1

# reset view to fit data
renderView1.ResetCamera()
renderView1.Update()

# save screenshot
SaveScreenshot('./T-XY.png', renderView1, ImageResolution=[1170, 796])









# Properties modified on slice1.SliceType
slice1.SliceType.Normal = [0.0, 1.0, 0.0]
slice1.SliceType.Origin = [x_room/2, y_room/2+0.001, z_room/2] # originXZ

# create a new 'Surface Vectors'
# surfaceVectors1 = SurfaceVectors(Input=slice1)
# set active source
# SetActiveSource(surfaceVectors1)
# surfaceVectors1Display = Show(surfaceVectors1, renderView1, 'GeometryRepresentation')
# set scalar coloring
# ColorBy(surfaceVectors1Display, ('POINTS', 'U', 'Magnitude'))
# get color transfer function/color map for 'U'
# uLUT = GetColorTransferFunction('U')
# rescale color and/or opacity maps used to include current data range
uLUT.RescaleTransferFunctionToDataRange()

# current camera placement for renderView1
renderView1.CameraPosition = [x_room/2, -x_room*4, z_room/2] # camposXZ
renderView1.CameraFocalPoint = [x_room/2, x_room/4, z_room/2] # camfocXZ
renderView1.CameraViewUp = [0.0, 0.0, 1.0]
renderView1.CameraParallelScale = 1

# reset view to fit data
renderView1.ResetCamera()
renderView1.Update()

# save screenshot
SaveScreenshot('./T-XZ.png', renderView1, ImageResolution=[1170, 796])
