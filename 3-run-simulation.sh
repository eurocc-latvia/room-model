#!/bin/bash

### divide in processors and run in parallel using slurm job manager (may take several hours)
sbatch slurm-job.sh

### or run in parallel without slurm
# chmod +x slurm-job.sh
# ./slurm-job.sh

### or run on one processor (may take more than 24 hours)
# buoyantSimpleFoam