#!/bin/bash
### set patch names
topoSet -dict system/topoSetDict_door
createPatch -dict system/createPatchDict_door -overwrite
topoSet -dict system/topoSetDict_window
createPatch -dict system/createPatchDict_window -overwrite
topoSet -dict system/topoSetDict_in
createPatch -dict system/createPatchDict_in -overwrite
topoSet -dict system/topoSetDict_out
createPatch -dict system/createPatchDict_out -overwrite
topoSet -dict system/topoSetDict_floor
createPatch -dict system/createPatchDict_floor -overwrite
topoSet -dict system/topoSetDict_ceiling
createPatch -dict system/createPatchDict_ceiling -overwrite
topoSet -dict system/topoSetDict_nose
createPatch -dict system/createPatchDict_nose -overwrite

### check mesh and create VTU folder for inspecting mesh with ParaView
rm -f 0/cellLevel
rm -f 0/pointLevel
checkMesh > log-checkMesh
foamToVTK

### make boundary conditions
python boundary-conditions.py
