difference(){
    union(){
        cube(size=[3, 3, 3]);
        translate(v=[-0.23, 1.05, 0]){
            cube(size=[0.231, 0.9, 2.11]);
        };
        translate(v=[2.999, 0.9, 1]){
            cube(size=[0.231, 1.2, 1.5]);
        };
    };
    translate(v=[-0.001, 1.05, 2.28]){
        difference(){
            cube(size=[0.231, 0.9, 0.3]);
            translate(v=[0, 0, -0.184]){
                rotate(a=[0, 45, 0]){
                    cube(size=[1, 1, 1]);
                };
            };
        };
    };
};
