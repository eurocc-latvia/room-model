#!/bin/bash

TARGET=custom-geometry.stl
INLET_SOURCE=in.stl
OUTLET_SOURCE=out.stl
WALL_SOURCE=walls.stl

[ -f $TARGET	 ] && rm $TARGET
touch $TARGET

sed -i '1 s/^.*$/solid in/' $INLET_SOURCE
cat $INLET_SOURCE >> $TARGET

sed -i '1 s/^.*$/solid out/' $OUTLET_SOURCE
cat $OUTLET_SOURCE >> $TARGET

sed -i '1 s/^.*$/solid walls/' $WALL_SOURCE
cat $WALL_SOURCE >> $TARGET

