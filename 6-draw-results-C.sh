#!/bin/bash
### evolution of C during the calculations
python plot-probes-C.py 

### C in the slice, defined in system/planeAnalysis file
cp ../parameters.py .
reconstructPar -fields "(C)" # -latestTime
pvpython draw-C.py 
# postProcess -func planeAnalysis # -latestTime
# python plot-surface-C.py 

### VTK folder will be created, with results that can be viewed with ParaView
foamToVTK -latestTime
