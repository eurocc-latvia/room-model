# GEOMETRY PARAMETERS ---------------------------------------------------------
# all geometry parameters are given in meters

x_room = 3 # size of the room along x axis
y_room = 3 # size of the room along y axis
z_room = 3 # size of the room along z axis (height)

x_door = 0.23 # size of the door frame along x axis (depth)
y_door = 0.9 # size of the door frame along y axis (width)
z_door = 2.11 # size of the door frame along z axis (height)
y_door_position = 1.05 # horizontal distance from the door to the front wall

x_win = 0.23 # size of the window frame along x axis (depth)
y_win = 1.2 # size of the window frame along y axis (width)
z_win = 1.5 # size of the window frame along z axis (height)
y_win_position = 0.9 # horizontal distance from the window to the front wall
z_win_position = 1 # vertical distance from the window to the floor

x_cond = 0.23 # size of the air conditioner along x axis (width)
y_cond = 0.9 # size of the air conditioner along y axis (length)
z_cond = 0.3 # size of the air conditioner along z axis (height)
y_cond_position = 1.05 # horizontal distance from the air conditioner to the front wall
z_cond_position = 2.28 # vertical distance from the air conditioner to the floor (should be greater than z_door)
condHoriz = 4*x_cond/5 # width of the horizontal surface at the bottom of air conditioner

y_outlet_position = y_room/2 # horizontal distance from the air outlet to the front wall
z_outlet_position = z_room-0.23 # vertical position of the air outlet
r_outlet = 0.11 # outlet radius

x_man_postion = 1.5 # distance from human to the left wall (x coordinate)
y_man_postion = 1.5 # distance from human to the front wall (y coordinate)



# BOUNDARY CONDITIONS -----------------------------------------------------

# for air velocity, no-slip condition is used on solid surfaces and fixed value 
# condition is imposed at the inlet:
U_inlet = 2 # velocity at the inlet, m/s
alpha_inlet = -70 # angle between inlet velocity and horizontal (+x) direction, degrees
U_nose = 0.1 # velocity of the human exhale (average in time), m/s
alpha_nose = -45 # angle between inlet velocity and horizontal (-y) direction, degrees

# for air temperature, 3rd type boundary condition is used at the walls:
#                         k dT/dn = h (Ta - T),
# where k is heat conductivity, dT/dn is normal gradient of temperature,
# Ta is ambient temperature and h is heat transfer coefficient.
Ta_wall = 284 # K
h_wall  = 8 # W/(m^2 K)
Ta_door = 284 # K
h_door  = 8 # W/(m^2 K)
Ta_win  = 284 # K
h_win   = 8 # W/(m^2 K)
T_in    = 296 # fixed temperature value at the inlet, K



# MESH PARAMETERS ---------------------------------------------------------

n_x = 40 # mesh density (number of cells in one meter) along x axis, approximate
n_y = 40 # mesh density (number of cells in one meter) along y axis, approximate
n_z = 40 # mesh density (number of cells in one meter) along z axis, approximate

layer = max(x_door, x_win)+0.01 # thickness of the layer near every wall in blockMeshDict, m
layer2 = 0.05 # thickness of the layer with refined (twice in normal direction) cells, m
rate = 2 # ratio of cell sizes in the center and near the wall 
r_cyl = 0.3 # radius of cylindrical region where mesh should be refined near the inlet
r_cyl2 = 0.1 # radius of the cylindrical region of additional mesh refinement
r_cyl_man = 0.1 # radius of the refined mesh region near the man's nose

n_proc = 16 # number of processors to run parallel calculations
