#### import the simple module from the paraview
from paraview.simple import *
from parameters import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'OpenFOAMReader'
afoam = OpenFOAMReader(FileName='./a.foam')

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
renderView1.ViewSize = [1170, 796]

# get layout
layout1 = GetLayout()

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Properties modified on afoam
afoam.CellArrays = ['U']



# create a new 'Slice'
slice1 = Slice(Input=afoam)

# show data in view
slice1Display = Show(slice1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
slice1Display.Representation = 'Surface'

# hide data in view
# Hide(afoam, renderView1)
# get display properties
afoamDisplay = GetDisplayProperties(afoam, view=renderView1)
# Properties modified on afoamDisplay
afoamDisplay.Opacity = 0.2

# Properties modified on slice1.SliceType
slice1.SliceType.Origin = [x_room/2+0.001, y_room/2, z_room/2] # originYZ
slice1.SliceType.Normal = [1.0, 0.0, 0.0]



# create a new 'Surface Vectors'
surfaceVectors1 = SurfaceVectors(Input=slice1)

# show data in view
surfaceVectors1Display = Show(surfaceVectors1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
surfaceVectors1Display.Representation = 'Surface'

# hide data in view
Hide(slice1, renderView1)




# create a new 'Glyph'
glyph1 = Glyph(Input=surfaceVectors1,
    GlyphType='Arrow')

# show data in view
glyph1Display = Show(glyph1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
glyph1Display.Representation = 'Surface'

# Properties modified on glyph1
glyph1.ScaleArray = ['POINTS', 'U']

# change solid color
glyph1Display.DiffuseColor = [0.0, 0.0, 0.0]

# Properties modified on glyph1
glyph1.ScaleFactor = 0.4
glyph1.GlyphType.TipLength = 0.55
glyph1.GlyphType.TipRadius = 0.2
glyph1.GlyphType.ShaftRadius = 0.06



# set active source
SetActiveSource(surfaceVectors1)

# set scalar coloring
ColorBy(surfaceVectors1Display, ('POINTS', 'U', 'Magnitude'))

# rescale color and/or opacity maps used to include current data range
surfaceVectors1Display.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
surfaceVectors1Display.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'U'
uLUT = GetColorTransferFunction('U')

# get opacity transfer function/opacity map for 'U'
uPWF = GetOpacityTransferFunction('U')

# Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
uLUT.ApplyPreset('Viridis (matplotlib)', True)

# Properties modified on uLUT
uLUT.NumberOfTableValues = 24

# get color legend/bar for uLUT in view renderView1
uLUTColorBar = GetScalarBar(uLUT, renderView1)
# Properties modified on uLUTColorBar
uLUTColorBar.Title = 'v,'
uLUTColorBar.ComponentTitle = 'm/s'




# current camera placement for renderView1
renderView1.CameraPosition = [-y_room*4, y_room/2, z_room/2] # camposYZ
renderView1.CameraFocalPoint = [y_room/4, y_room/2, z_room/2] # camfocYZ
renderView1.CameraViewUp = [0.0, 0.0, 1.0]
renderView1.CameraParallelScale = 1

# reset view to fit data
renderView1.ResetCamera()
renderView1.Update()

# save screenshot
SaveScreenshot('./v-YZ.png', renderView1, ImageResolution=[1170, 796])









# Properties modified on slice1.SliceType
slice1.SliceType.Normal = [0.0, 0.0, 1.0]
slice1.SliceType.Origin = [x_room/2, y_room/2, z_room/2+0.001] # originXY

# create a new 'Surface Vectors'
surfaceVectors1 = SurfaceVectors(Input=slice1)
# set active source
SetActiveSource(surfaceVectors1)
surfaceVectors1Display = Show(surfaceVectors1, renderView1, 'GeometryRepresentation')
# set scalar coloring
ColorBy(surfaceVectors1Display, ('POINTS', 'U', 'Magnitude'))
# get color transfer function/color map for 'U'
uLUT = GetColorTransferFunction('U')
# rescale color and/or opacity maps used to include current data range
uLUT.RescaleTransferFunctionToDataRange()

# current camera placement for renderView1
renderView1.CameraPosition = [x_room/2, y_room/2, z_room*5.2] # camposXY
renderView1.CameraFocalPoint = [x_room/2, y_room/2, 3*z_room/4] # camfocXY
renderView1.CameraViewUp = [0.0, 1.0, 0.0]
renderView1.CameraParallelScale = 1

# reset view to fit data
renderView1.ResetCamera()
renderView1.Update()

# save screenshot
SaveScreenshot('./v-XY.png', renderView1, ImageResolution=[1170, 796])









# Properties modified on slice1.SliceType
slice1.SliceType.Normal = [0.0, 1.0, 0.0]
slice1.SliceType.Origin = [x_room/2, y_room/2+0.001, z_room/2] # originXZ

# create a new 'Surface Vectors'
surfaceVectors1 = SurfaceVectors(Input=slice1)
# set active source
SetActiveSource(surfaceVectors1)
surfaceVectors1Display = Show(surfaceVectors1, renderView1, 'GeometryRepresentation')
# set scalar coloring
ColorBy(surfaceVectors1Display, ('POINTS', 'U', 'Magnitude'))
# get color transfer function/color map for 'U'
uLUT = GetColorTransferFunction('U')
# rescale color and/or opacity maps used to include current data range
uLUT.RescaleTransferFunctionToDataRange()


# get active source.
glyph1 = FindSource('Glyph1')
# Properties modified on glyph1
glyph1.ScaleFactor = 0.1


# current camera placement for renderView1
renderView1.CameraPosition = [x_room/2, -x_room*4, z_room/2] # camposXZ
renderView1.CameraFocalPoint = [x_room/2, x_room/4, z_room/2] # camfocXZ
renderView1.CameraViewUp = [0.0, 0.0, 1.0]
renderView1.CameraParallelScale = 1

# reset view to fit data
renderView1.ResetCamera()
renderView1.Update()

# save screenshot
SaveScreenshot('./v-XZ.png', renderView1, ImageResolution=[1170, 796])