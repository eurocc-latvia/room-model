#### import the simple module from the paraview
from paraview.simple import *
from parameters import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'OpenFOAMReader'
afoam = OpenFOAMReader(FileName='./a.foam')

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
renderView1.ViewSize = [1170, 796]

# get layout
layout1 = GetLayout()

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Properties modified on afoam
afoam.CellArrays = ['C']



# create a new 'Slice'
slice1 = Slice(Input=afoam)

# show data in view
slice1Display = Show(slice1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
slice1Display.Representation = 'Surface'

# hide data in view
# Hide(afoam, renderView1)
# get display properties
afoamDisplay = GetDisplayProperties(afoam, view=renderView1)
# Properties modified on afoamDisplay
afoamDisplay.Opacity = 0.2

# Properties modified on slice1.SliceType
slice1.SliceType.Origin = [x_room/2, y_room/2+0.001, z_room/2] # originXZ
slice1.SliceType.Normal = [0.0, 1.0, 0.0]






# set active source
SetActiveSource(slice1)

# set scalar coloring
ColorBy(slice1Display, ('POINTS', 'C'))

# rescale color and/or opacity maps used to include current data range
slice1Display.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
slice1Display.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'C'
cLUT = GetColorTransferFunction('C')

# get opacity transfer function/opacity map for 'C'
CPWF = GetOpacityTransferFunction('C')

# Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
cLUT.ApplyPreset('Inferno (matplotlib)', True)
cLUT.NumberOfTableValues = 24
cLUT.InvertTransferFunction()
cLUT.RescaleTransferFunction(0.0001, 0.5)
cLUT.MapControlPointsToLogSpace()
cLUT.UseLogScale = 1

# get color legend/bar for CLUT in view renderView1
CLUTColorBar = GetScalarBar(cLUT, renderView1)
# Properties modified on uLUTColorBar
CLUTColorBar.Title = 'C,'
CLUTColorBar.ComponentTitle = 'arb.u.'




# create a new 'Annotate Time'
annotateTime1 = AnnotateTime()

# show data in view
annotateTime1Display = Show(annotateTime1, renderView1, 'TextSourceRepresentation')

# Properties modified on annotateTime1
annotateTime1.Format = 'Time: {time:.0f} s'

# Properties modified on annotateTime1Display
annotateTime1Display.FontSize = 18

# Properties modified on annotateTime1Display
annotateTime1Display.WindowLocation = 'Upper Center'




# current camera placement for renderView1
renderView1.CameraPosition = [x_room/2, -x_room*4, z_room/2] # camposXZ
renderView1.CameraFocalPoint = [x_room/2, x_room/4, z_room/2] # camfocXZ
renderView1.CameraViewUp = [0.0, 0.0, 1.0]
renderView1.CameraParallelScale = 1

# reset view to fit data
renderView1.ResetCamera()
renderView1.Update()

# save screenshot
# animationScene1 = GetAnimationScene()
# animationScene1.GoToFirst()
# for i in range(0,4):
    # SaveScreenshot(f'./C-timestep-{i}.png', renderView1, ImageResolution=[1170, 796])
    # animationScene1.GoToNext()
SaveAnimation('./C.timestep.png', renderView1, ImageResolution=[1170, 796])
SaveAnimation('./C.timestep.avi', renderView1, ImageResolution=[1170, 796])

