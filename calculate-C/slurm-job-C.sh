#!/bin/bash
#SBATCH --job-name=EuroCC-demo_aerosol  # Job name
#SBATCH --ntasks=16                     # Number of tasks
#SBATCH --partition=regular             # Partition name

echo "Current dir = "; pwd; hostname; date
decomposePar -force
mpirun -np 16 scalarTransportTurbulentFoam -limit -parallel >./log-run
echo ""; echo ""; echo ""; echo "" 
echo "Simulations are completed! Current time:"
date

echo ""; echo ""; echo ""; echo "" 
echo "Drawing results:"
cp ../6-draw-results-C.sh .
./6-draw-results-C.sh
echo ""; echo ""; echo ""; echo "" 
echo "Simulations and visualisation are completed! Current time:"
date
