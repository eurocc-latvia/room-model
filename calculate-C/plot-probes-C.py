import matplotlib.pyplot as plt
import numpy as np

probes_door = np.loadtxt('postProcessing/probes_door/0/C')
probes_window = np.loadtxt('postProcessing/probes_window/0/C')

plt.figure(figsize=(10,5))
plt.plot(probes_door[:,0]/60, probes_door[:,1], label='door')
plt.plot(probes_window[:,0]/60, probes_window[:,1], label='window')
ax = plt.gca()
ax.set_xlabel('t, min')
ax.set_ylabel('C, arb.u.')
plt.legend()
plt.savefig('probes-C.png')
