#!/bin/bash

### run python script that creates instructions for mesh generation (OpenSCAD, blockMesh and snappyHexMesh)
python geometry.py

### use OpenSCAD to create stl files:
# mkdir -p constant/triSurface
openscad geometry.scad --render -o constant/triSurface/allSurfaces.stl
openscad geometry-man.scad --render -o constant/triSurface/coarse-man-scaled.stl

### make a cube, inisde which the geometry is contained
blockMesh

### cut stl geometry from the cube, refine mesh near the surfaces
snappyHexMesh -overwrite
topoSet -dict system/topoSetDict_REF_walls
refineMesh -overwrite -dict system/refineMeshDictX
refineMesh -overwrite -dict system/refineMeshDictY
refineMesh -overwrite -dict system/refineMeshDictZ

### draw mesh
pvpython draw-mesh.py
