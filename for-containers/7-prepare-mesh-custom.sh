#!/bin/bash

# please prepare an ASCII stl file in constant/triSurface/custom-geometry.stl
# with surface names in, out, walls (required) 
# and window, door, floor, ceiling (optional)

### run python script that creates instructions for mesh generation (OpenSCAD, blockMesh and snappyHexMesh)
python geometry-custom-stl.py

### use OpenSCAD to create stl files:
openscad geometry-man.scad --render -o constant/triSurface/coarse-man-scaled.stl

### make a cube, inisde which the geometry is contained
blockMesh

### cut stl geometry from the cube, refine mesh near the surfaces
snappyHexMesh -overwrite -dict system/snappyHexMeshDictCustom

### draw mesh
pvpython draw-mesh.py

### prepare boundary conditions
rm -f 0/cellLevel
rm -f 0/pointLevel
topoSet -dict system/topoSetDict_nose
createPatch -dict system/createPatchDict_nose -overwrite
checkMesh > log-checkMesh
foamToVTK
python boundary-conditions.py

# please change boundary conditions for velocity (in the file 0/U)
# and probe locations (in files system/controlDict and calculate-C/system/controlDict)
