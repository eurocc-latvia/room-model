#### import the simple module from the paraview
from paraview.simple import *
from parameters import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'OpenFOAMReader'
afoam = OpenFOAMReader(FileName='./a.foam')
afoam.SkipZeroTime = 0
afoam.MeshRegions = ['internalMesh']

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
renderView1.ViewSize = [1170, 796]

# get layout
layout1 = GetLayout()

# show data in view
afoamDisplay = Show(afoam, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
afoamDisplay.Representation = 'Surface'

# reset view to fit data
renderView1.ResetCamera()

# get the material library
materialLibrary1 = GetMaterialLibrary()

# create a new 'Clip'
clip1 = Clip(Input=afoam)

# Properties modified on clip1.ClipType
clip1.ClipType.Origin = [x_room/2+0.001, y_room/2, z_room/2] # originYZ

# Properties modified on clip1
clip1.Scalars = ['POINTS', '']

# show data in view
clip1Display = Show(clip1, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
clip1Display.Representation = 'Surface'

# hide data in view
Hide(afoam, renderView1)

# Properties modified on clip1
clip1.Crinkleclip = 1
clip1.Invert = 0

# change representation type
clip1Display.SetRepresentationType('Surface With Edges')

# toggle 3D widget visibility (only when running from the GUI)
Hide3DWidgets(proxy=clip1.ClipType)

# reset view to fit data
renderView1.ResetCamera()

# current camera placement for renderView1
renderView1.CameraPosition = [-y_room*4, y_room/2, z_room/2] # camposYZ
renderView1.CameraFocalPoint = [y_room/4, y_room/2, z_room/2] # camfocYZ
renderView1.CameraViewUp = [0.0, 0.0, 1.0]
renderView1.CameraParallelScale = 1

# reset view to fit data
# renderView1.ResetCamera()

# update the view to ensure updated data information
# renderView1.Update()

### + add axis

# save screenshot
SaveScreenshot('./mesh-YZ.png', renderView1, ImageResolution=[1170, 796])





# Properties modified on clip1.ClipType
clip1.ClipType.Normal = [0.0, 1.0, 0.0]
clip1.ClipType.Origin = [x_room/2, y_room/2+0.001, z_room/2] # originXZ

# Properties modified on clip1
clip1.Invert = 0

# reset view to fit data
renderView1.ResetCamera()

# current camera placement for renderView1
renderView1.CameraPosition = [x_room/2, -x_room*4, z_room/2] # camposXZ
renderView1.CameraFocalPoint = [x_room/2, x_room/4, z_room/2] # camfocXZ
renderView1.CameraViewUp = [0.0, 0.0, 1.0]
renderView1.CameraParallelScale = 1

# reset view to fit data
# renderView1.ResetCamera()

# update the view to ensure updated data information
# renderView1.Update()

# save screenshot
SaveScreenshot('./mesh-XZ.png', renderView1, ImageResolution=[1170, 796])



# Properties modified on clip1.ClipType
clip1.ClipType.Normal = [0.0, 0.0, 1.0]
clip1.ClipType.Origin = [x_room/2, y_room/2, z_room/2+0.001] # originXY

# Properties modified on clip1
clip1.Invert = 1

# reset view to fit data
renderView1.ResetCamera()

# current camera placement for renderView1
renderView1.CameraPosition = [x_room/2, y_room/2, z_room*5.2] # camposXY
renderView1.CameraFocalPoint = [x_room/2, y_room/2, 3*z_room/4] # camfocXY
renderView1.CameraViewUp = [0.0, 1.0, 0.0]
renderView1.CameraParallelScale = 1

# reset view to fit data
# renderView1.ResetCamera()

# update the view to ensure updated data information
# renderView1.Update()

# save screenshot
SaveScreenshot('./mesh-XY.png', renderView1, ImageResolution=[1170, 796])
