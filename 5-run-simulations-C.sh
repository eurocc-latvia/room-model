#!/bin/bash

### copy necessary files and navigate into C simulation directory
python copy-files-for-C.py
cd 'calculate-C/'

### divide in processors and run in parallel using slurm job manager (may take several hours)
sbatch slurm-job-C.sh

### or run in parallel without slurm
# chmod +x slurm-job-C.sh
# ./slurm-job-C.sh

### or run on one processor (may take more than 24 hours)
# scalarTransportTurbulentFoam -limit

