# Summary

This demonstration case models quasi-stationary air flow and air temperature, then calculates transient distribution of aerosol concentration in a room. The ventilation is modelled by setting constant air velocity at the inlet of the air conditioner. Using the obtained air flow pattern, aerosol concentration simulations are performed. It is assumed that aerosols are produced by a human with a constant breathing rate. Quick tutorial on basic functionality is [available here](https://www.youtube.com/watch?v=Mf0JPYAzsa0), however you should continue reading this *README* file in order to understand the calculations in more details. 

For air flow simulation, *k-w*  SST turbulence model is used, the calculated turbulent diffusivity is taken into account in concentration simulation. Both mesh generation and physics simulations are performed using the *[OpenFOAM](https://openfoam.org/)* library. For the preparation of the case geometry, *[OpenSCAD](https://openscad.org/)* is used. Automatization of pre- and post-processing (for improving convenience for first-time users) is realized by *Python* and *shell* scripts.

This demonstration can be used for simplified evaluation of thermal comfort (by analyzing temperature differences between different parts of the room) and epidemiological safety (by investigating ventilation influence on the evolution of aerosol concentration) in a small or medium-sized room. Another purpose is to demonstrate the typical workflow and steps of HPC calculations. 

**This HPC demonstration was prepared as a part of the training and dissemination activities organized by the [EuroCC National HPC Competence Centre of Latvia](https://eurocc-latvia.lv/?lang=en).**


# Table of contents
[[_TOC_]]

# Problem background
This case considers a rectangular room with a door, a window, an air conditioner and a human figure. The air conditioner can also be used as a heater, if temperature value at its outlet is set higher than the temperature of the walls. Air velocity *v* and temperature *T* are calculated, see Figure below. Using the obtained velocity field and turbulent diffusion coefficient, time-dependent aerosol concentration *C* (normalized such that near the head of the human *C* = 1 arbitrary unit) is calculated, and its evolution in time is demonstrated in the animation below the figure. 

To get these visualizations, *[ParaView](https://www.paraview.org/download/)* program is required. However, simpler vizualization of results can also be performed automatically after the calculations, see Section [Vizualization of obtained results](#running-quasi-stationary-air-flow-simulation).

<img src="img_README/all.png" width="650" alt="Example of obtained results: air velocity, temperature, aerosol concentration">

![Aerosol C animation](img_README/video.gif)

Default room parameters correspond to the case described in a [paper by A. Sabanskis and J. Virbulis](https://www.researchgate.net/publication/307668266_Experimental_and_Numerical_Analysis_of_Air_Flow_Heat_Transfer_and_Thermal_Comfort_in_Buildings_with_Different_Heating_Systems) about air flow in an experimental buliding with the inner size of 3×3×3 m<sup>3</sup>. The paper is also used for qualitative verification of this demo-case, which corresponds to *aa1* case (top left image) form Figure 3 in the paper.

The desigations of system geometrical parameters (black) and names of the boundaries (red) are shown in the image below. These parameters can be changed in `parameters.py` file, see [Mesh generation](#mesh-generation).

![Geometrical parameters of the room](img_README/sketch.png)

Boundary conditions for air velocity are:
- Fixed velocitites at `in` and `nose`
- *OpenFOAM* inlet-outlet condition (equivalent to zero gradient, or free outflow) at `out`
- No-slip condition at all other surfaces

Boundary conditions for air temperature are:
- Fixed temperatures at `in`, `nose` and human figure (called `patchMan` in *OpenFOAM* files)
- *OpenFOAM* inlet-outlet condition (equivalent to zero gradient, or perfect insulation) at `out`
- Zero gradient condition at `floor` and `ceiling`
- Third type boundary conditions at `window`, `door` and walls (called `".*"` in *OpenFOAM* files)

Boundary conditions for aerosol concentration in the case of an infectios person are:
- *C* = 1 arbitrary unit at `nose`
- *C* = 0 at `in` (fresh air is assumed to flow out of air conditioner)
- Zero gradient condition at all other surfaces
- Initial condition of *C* = 0 in the volume of the room

The modifications for modelling *C* decrease in a room with a non-infectios person are described in [Running transient simulation of aerosol concentration](#running-transient-simulation-of-aerosol-concentration).

# Contents of this case
Here is the overview of all files in this repository. To run this simulation, it is *not* required to understand the meaning of each file in details.
* `parameters.py` — the file that contains main user-defined geometrical, physical and mesh-related parameters of the simulation.
* *OpenFOAM* simulation files:
  * `0` — Initial conditions for air velocity, air temperature and other physical fields
  * `constant` — Material properties and mesh
  * `system` — Various parameters of *OpenFOAM* calculations, pre- and post-processing options
    * `controlDict` — Time step, calculation lenght and other calculation controls
    * `planeAnalysis`, `singleGraph` — Post-processing options for data vizualization on a user-defined plane and line, correspondingly
    * `createPatchDict_*`, `topoSetDict_*` — Instructions for the creation of boundary patches
    * `fvSchemes`, `fvSolution` — Parameters of solvers and numerical schemes
    * `blockMeshDict`, `snappyHexMeshDict` — Instructions for mesh generation
  * `calculate-C` — a folder with *OpenFOAM* simulation files for aerosol concentration, structured in the same way as described before
* `scalarTransportTurbulentFoam` — custom solver for aerosol concentration
* `.sh` scripts — scripts that prepare and run simulations 
* `.py` scripts — scripts that are invoked by `.sh` scripts to define the geometry of the domain, boundary conditions, and to draw the results
* `.scad` files — the files that contains geometry of the room and the human, are created by `geometry.py` and used as instructions for *OpenSCAD*
* `OpenSCAD_container.sif` — *singularity* container with *OpenSCAD* program for the generation of `constant/triSurface/allSurfaces.stl` file, which is then used by `snappyHexMesh`
* `slurm-job` — Batch script for submitting the simulation job to an HPC cluster where [*Slurm*](https://slurm.schedmd.com/sbatch.html) workload manager is installed
* `README.md` — This *readme* file
* `img_README` — Folder containing images for this *readme* file

# Running the case on an HPC cluster
Use the instruction from this chapter to run simulations **without** root rights. In this case all necessary modules (*conda*, *OpenFOAM*, *singularity*) should **already be installed**. This requriement is satisfied at LU and RTU clusters. For other clusters, **at least** *singularity* software should be installed.

## Obtaining the case

To be able to connect to HPC cluster, you need to download and install appropriate software, for example:
* [*PuTTy*](https://www.putty.org/) to execute commands in terminal (command line)
* [*WinScp*](https://winscp.net/eng/download.php) to view and copy files

After these programs are installed, open each of them and log in using your username and password. Then download this calculation case directly from GitLab by executing the command below.
```
git clone https://gitlab.com/eurocc-latvia/room-model.git
```
Alternatively, the code can be downloaded as `.zip` or `.tar` archive, copied to cluster by *WinScp*, and then un-archived by `unzip` or `tar -xzvf`.

## Preparation of the environment

**Loading software modules**

Navigate to the main directory of the case and change permissions of `.sh` scripts:
```
cd room-model
chmod +x *.sh
```

Execute the following command to load modules that are pre-installed on the cluster:
```
source 0-prepare-environment.sh
```

**If** any of the required programs (*openfoam-6.0*, *mpirun*, *singularity*, *conda*) is not found, then
1. Exit the `source` process by pressing *Ctrl-C*
2. Contact your system administrator and ask to install them all. After all programs are installed, repeat the `source 0-prepare-environment.sh` command and continue to the subsection "***conda*** **environment**" below.
3. If point 2. is not possible, ask them to install at least *singularity*
4. When *singularity* is available for you, run the following command to pull the container:
```
singularity pull OpenFOAM_OpenSCAD_conda_ParaView.sif library://kirils.surovovs/default/all-public.sif:sha256.a2cc86c581be772e35d5c667b10f027e20ea80562ae1baa8fedddd0525fc640b
```
or download the container from [this page](https://cloud.sylabs.io/library/_container/60facd36d63fe43757fac448) and save as `OpenFOAM_OpenSCAD_conda_ParaView.sif`. The container provides all necessary programs, as indicated in its filename.

5. Open a shell inside the container by running `singularity shell OpenFOAM_OpenSCAD_conda_ParaView.sif`. Then prepare the environment with these commands:
```
cp for-containers/* .
chmod +x *.sh
source /opt/openfoam6/etc/bashrc
PATH=/ParaView-5.9.1-osmesa-MPI-Linux-Python3.8-64bit/bin:$PATH
PATH=/opt/conda/bin/:$PATH
```
6. Finally, compile the custom solver by executing:
```
cd scalarTransportTurbulentFoam
wclean && wmake
cd ..
```
and then continue to the section [Mesh generation](#mesh-generation) of this *README* file.



***conda*** **environment**
- **If you already have more than one *conda* environment**, then input the name of the environment that you would like to use and press *Enter*. 
- **If there are no *conda* environments** except `base`, then exit the `source` process by pressing *Ctrl-C* and then create a new environment by running
```
conda create --name EuroCC
```
Then source the preparation script again:
```
source 0-prepare-environment.sh
```
and select the `EuroCC` environment.
- **If the instructions on the screen are requiring to do `conda init`**, you may also need to run
```
conda init bash
conda activate EuroCC
```

***Python*** **modules**

When *conda* environment is activated (its name should appear in the terminal before the title of the current directory), install the required *python* modules by running
```
conda install pip
pip install  numpy numpy-stl pandas matplotlib openpyscad
```

**Compilation of *scalarTransportTurbulentFoam* solver**

As there are no standard *OpenFOAM* solvers for mass transfer with non-uniform diffusion coefficient, the custom solver should be compiled by executing:
```
cd scalarTransportTurbulentFoam
wclean && wmake
cd ..
```
To check if the solver is compiled correctly, type `which scalarTransportTurbulentFoam`, and the path to the solver executable should be returned.

The last two steps (*python* module installation and solver compilation) should be performed only once. The sourcing of the working environment by running `source 0-prepare-environment.sh` should be performed *after every login*.

## Mesh generation
The main geometrical parameters of the case, as well as some mesh parameters, are defined in `parameters.py` file. The meaning of the variable names is given in [Problem background](#problem-background). To prepare mesh using these parameters, execute the following command:
```
./1-prepare-mesh.sh
```
Execution of this command may take from several minutes to several hours, depending on geometrical parameters and mesh size. After the mesh is ready, `mesh-*.png` files will appear in the current directory. They show mesh slices in three planes (*xz*, *yz* and *xy*) and allow to check if the mesh looks good before continuing to the next step. An example of these images is shown below (here coarse mesh is shown, default parameters should produce finer mesh):

![An example of created mesh](img_README/mesh.png)

## Preparation of boundary conditions
Boundary condition properties (inlet velocities, wall temperatures and heat transfer coefficients), as well as number of processors to use for simulations, are defined in `parameters.py` file. To prepare boundary conditions, execute the command:
```
./2-prepare-boundary-conditions.sh
```
It should complete in several minutes. After the command is completed, the obtained mesh can be viewed in *[ParaView](https://www.paraview.org/download/)* by opening the files in `VTK` folder. Statistics about the mesh quality is written into `log-checkMesh` file. Small problems with the mesh (300-500 highly non-orthogonal faces, several highly skewed faces) likely may not obstruct simulations. If the problems are more severe, try to run the simulation (as described in the next section) anyway. If the simulation crashes, change mesh parameters in `parameters.py`. The mesh refinement levels are given in `system/snappyHexMeshDict` file, lines 293-303.

## Running quasi-stationary air flow simulation
To run air flow simulation on a cluster where *slurm* job manager is installed, simply type:
```
./3-run-simulation.sh
```
The calculations probably will be rather long, at least several hours. However, the preliminary results can be obtained, see the **Vizualization** sections below. By default, calculations will be run in parallel, the number of processors is defined in the last line of `parameters.py`. To run parallel simulations without *slurm*, execute `chmod +x slurm-job.sh && ./slurm-job.sh >log-decompose &`. It is also possible (but not recommended) to run calculations without parallelization by executing `buoyantSimpleFoam`, however in this case simulation is likely to take several days.

**Simulation control**

Simulation should automatically end if `endTime`, defined in `system/controlDict` file, is reached, or if residuals of all fields are smaller, than the thresholds defined in lines 60-62 of `system/fvSolution` file. The mentioned files can be modified while simulation is still running. 

When the simulation **is completed**, the corresponding message will appear in the end of `slurm-...` logfile (if *slurm* was used to run the job, e.g. on LU cluster) or `log-decompose` logfile (if the job was run witout *slurm* or using containers), and the results visualisation process will be automatically started, creating `.png` and `VTK` files in the current directory. Note that the images in `v-*.png` show the *projection* of air velocity on the corresponding plane, not full velocity magnitude.

Additional information about simulation progress is provided in `log-run` file. It contains *OpenFOAM* output: reporting of solver preparation, physical parameters, and residuals at each timestep. However, more convenient way of monitoring simulation progress is described in the next section.

**Vizualization of simulation progress**

To check calculation progress before it is completed, execute
```
python plot-probes.py
```
Two `.png` files with time-dependencies of air temperature and velocity will be created. Probe locations are defined at the end of `system/controlDict` file, and by default they are put near the middle of the door and the window. Do not worry about non-realistic results at the beginning of the simulation -- a quasi-stationary solver is used, therefore it does not describe the evolution of air flow in time and should converge at realistic solution only at the end. 

**Vizualization of obtained results**

When simulation is completed, the results will be automatically vizualized. However, preliminary results are saved during the simulations with a frequency defined by `writeInterval` in `system/controlDict` file. Thus it is possible to draw preliminary results by running
```
./4-draw-results.sh
```
The coordinates of plane and line for the sampling of the results are given in files `system/planeAnalysis` and `system/singleGraph`. In the current directory, `.png` files will be created, and in the `VTK` directory, fields from the latest saved time step will be written. To open the files from `VTK` directory, *ParaView* program is necessary.

## Running transient simulation of aerosol concentration
After the simulation of air flow are completed (or at least when the results are reconstructed for any time that is greater than zero), it is possible to run aerosol concentration simulation by
```
./5-run-simulations-C.sh
```
This simulation is also controlled by `calculate-C/system/controlDict` file, as described in the previous section. When the simulation is completed, the corresponding message will appear in `calculate-C/slurm-...` logfile. However, in this case, unlike the air flow case from the previous section, transient solver is used. It means that the evolution of *C* in time is described, and it is not necessary to wait until simulation ends to obtain realistic results.

**Initial conditions and boundary conditions**

By default, initial conditions of aerosol concentration are *C*=0 in the whole room, conditioner acts as a fresh air inlet, and the person is infected, thus the increase of *C* in the ventilated occupied room is being modelled. 

However, it is also possible to model another scenario: the concentration decrease in a ventilated unoccupied room. It corresponds to the case when there had been some activity with at least one infected participant, and after the end of this activity people left and *C* starts to decrease. In this case, a human does not emit aerosol. To model the described situation, modify `calculate-C/0/C` file in the following way:
1. Change `internalFiled` to `uniform 1`
2. Change *C* value for `nose` boundary to `uniform 0`
3. Navigate to the main folder and run *C* simulation again by executing `./5-run-simulations-C.sh`

**Vizualization of calculation progress**

To check calculation progress (concentration at probe locations, defined at the end of `calculate-C/system/controlDict` file) before simulations are completed, execute
```
cd calculate-C
python plot-probes-C.py
```

**Vizualization of obtained results**

It is also possible to draw preliminary or final results by running
```
cd calculate-C
cp ../6-draw-results-C.sh .
./6-draw-results-C.sh
```
The aerosol concentration from the last step will be saved in `VTK` directory, evolution of *C* in a slice plane (default is *xz* plane, its properties are defined in line 43 of `calculate-C/draw-C.py` file) will be saved in `*.png` and `*.avi` files.


# Using custom .stl file for room geometry

To use a custom, user-provided `.stl` file, the following steps should be performed:
1. Start in a new, clean calculation folder, e.g. by cloning the repository again:
```
mkdir new-case
cd new-case
git clone https://gitlab.com/eurocc-latvia/room-model.git
cd room-model
chmod +x *.sh
```
2. [Prepare the environment](#preparation-of-the-environment), if necessary.
3. Copy your `.stl` file into `constant/triSurface/custom-geometry.stl`. This file can be prepared in any CAD software (*Salome*, *AutoCAD* etc.), however there are some requirements for this file:  
  - it must be in ASCII format (not binary),
  - it must contain surfaces with names `in`, `out`, and `walls`,
  - it may optionally contain surfaces with names `window`, `door`, `floor`, or `ceiling`,
  - if you have separate `.stl` files for different surfaces, you can unite them by executing `cd constant/triSurface` and running `chmod +x uniteSTL.sh && ./uniteSTL.sh`, in which the required filenames should be specified in lines 4-6. To use optional surface names, copy `sed` and `cat` commands from lines 17-18 and replace "walls" with the optional surface name.
4. Modify the necessary parameters in `parameters.py`. Please note that **none** of the geometrical parameters from this file will be used in this case (i.e., with user-supplied `.stl` file), except man position.
5. Run the mesh preparation script: `./7-prepare-mesh-custom.sh`, the execution of which may take from several minutes to one hour. After it is completed, mesh can be viewed in the `VTK` folder, and `mesh-*.png` files will appear in the current directory. Please examine these files to check if mesh has been generated correctly. If the mesh is created outside the `.stl` (and not inside it), then return to step **1** and then manually change the `locationInMesh` string in line 172 of `geometry-custom-stl.py`, which should contain coordinates of an arbitrary point *inside* the room.
6. Change manually boundary conditions for velocity in `0/U` file, in the line where `U_in` is defined.
7. Change manually the positions of probe points by editing `probeLocations` in `system/controlDict` and `calculate-C/system/controlDict`.
8. [Run the quasi-stationary simulation](#running-quasi-stationary-air-flow-simulation).
9. If it does not converge, try to decrease `relaxationFactors` in `system/fvSolution`. If it does not help, return to step **1** (it is important to copy a "clean" case before running the mesh preparation script) and repeat these steps using different mesh parameters in `parameters.py`. 
10. After the quasi-stationary simulation is finished, [run the aerosol concentration simulation](#running-transient-simulation-of-aerosol-concentration).


# Running on a PC
Use the instruction in this chapter to run simulations on PC (i.e., **with** root rights). This method is not recommended and should be used only if HPC resources are not available. 

## Installing *singularity*

This section is based on [this installation guide](https://sylabs.io/guides/3.7/user-guide/quick_start.html). In this example, *Ubuntu* system is used. Installation on *Windows* [may be possible](https://sylabs.io/guides/3.7/admin-guide/installation.html#installation-on-windows-or-mac), but has not been tested for this demo-case.

1. Install the necessary dependencies first:
```
sudo apt-get update && sudo apt-get install -y \
    build-essential \
    libssl-dev \
    uuid-dev \
    libgpgme11-dev \
    squashfs-tools \
    libseccomp-dev \
    wget \
    pkg-config \
    git \
    cryptsetup
```
2. Download *go* language from [golang.org](https://golang.org/dl/)
3. Extract the archive and add *go* to *PATH*:
```
sudo tar -C /usr/local -xzf go1.16.linux-amd64.tar.gz
export PATH=$PATH:/usr/local/go/bin
source $HOME/.profile
```
4. Download *singularity* source code from [GitHub](https://github.com/hpcng/singularity/releases) and save it into your `$HOME` directory
5. Install *singularity* with these commands (replace `3.8.0` with the current version number):
```
cd ~
tar -xzf singularity-3.8.0.tar.gz
cd singularity-3.8.0
./mconfig -b ./buildtree -p /usr/local
cd buildtree
make
sudo make install
```
6. Check if installation was successful by executing `singularity version`


## Preparation of the environment

1. When *singularity* is installed, run the following command to pull the container:
```
singularity pull OpenFOAM_OpenSCAD_conda_ParaView.sif library://kirils.surovovs/default/all-public.sif:sha256.a2cc86c581be772e35d5c667b10f027e20ea80562ae1baa8fedddd0525fc640b
```
or download the container from [this page](https://cloud.sylabs.io/library/_container/60facd36d63fe43757fac448) and save as `OpenFOAM_OpenSCAD_conda_ParaView.sif`. The container provides all necessary programs, as indicated in its filename.

2. Open a shell inside the container by running `singularity shell OpenFOAM_OpenSCAD_conda_ParaView.sif`. Then download the calculation case and prepare the environment with these commands:
```
git clone https://gitlab.com/eurocc-latvia/room-model.git
cd room-model
cp for-containers/* .
chmod +x *.sh
source /opt/openfoam6/etc/bashrc
PATH=/ParaView-5.9.1-osmesa-MPI-Linux-Python3.8-64bit/bin:$PATH
PATH=/opt/conda/bin/:$PATH
```
3. Set a processor number in the last line of `parameters.py` file that corresponds to your PC (it can be obtained by executing `nproc` command).
4. Finally, compile the custom solver by executing:
```
cd scalarTransportTurbulentFoam
wclean && wmake
cd ..
```
and then continue to the section [Mesh generation](#mesh-generation) of this *README* file.
