#!/bin/bash

### run python script that creates instructions for mesh generation (OpenSCAD, blockMesh and snappyHexMesh)
python geometry.py

### use OpenSCAD to create stl files:
# mkdir -p constant/triSurface
singularity exec OpenSCAD_container.sif openscad geometry.scad --render -o constant/triSurface/allSurfaces.stl
singularity exec OpenSCAD_container.sif openscad geometry-man.scad --render -o constant/triSurface/coarse-man-scaled.stl

### make a cube, inisde which the geometry is contained
blockMesh

### cut stl geometry from the cube, refine mesh near the surfaces
sbatch slurm-snappyHexMesh.sh
