from parameters import *
from headers import *
import numpy as np

# air velocity
file = '0/U'
radians = alpha_inlet*3.1415/180 
ux = U_inlet*np.cos(radians)
uz = U_inlet*np.sin(radians)
stringToInsert = '        value           uniform ({0} {1} {2}); //U_in\n'.format(ux, 0, uz)
replaceIn(file, 'U_in', stringToInsert)
radians = alpha_nose*3.1415/180 
uy = U_nose*np.cos(radians)
uz = U_nose*np.sin(radians)
stringToInsert = '        value           uniform ({0} {1} {2}); //U_nose\n'.format(0, -uy, uz)
replaceIn(file, 'U_nose', stringToInsert)

# air temperature
file = '0/T'
stringToInsert = '        Ta              uniform {0}; //Ta_wall\n'.format(Ta_wall)
replaceIn(file, 'Ta_wall', stringToInsert)
stringToInsert = '        h               uniform {0}; //h_wall\n'.format(h_wall)
replaceIn(file, 'h_wall', stringToInsert)
stringToInsert = '        Ta              uniform {0}; //Ta_door\n'.format(Ta_door)
replaceIn(file, 'Ta_door', stringToInsert)
stringToInsert = '        h               uniform {0}; //h_door\n'.format(h_door)
replaceIn(file, 'h_door', stringToInsert)
stringToInsert = '        Ta              uniform {0}; //Ta_win\n'.format(Ta_win)
replaceIn(file, 'Ta_win', stringToInsert)
stringToInsert = '        h               uniform {0}; //h_win\n'.format(h_win)
replaceIn(file, 'h_win', stringToInsert)
stringToInsert = '        value           uniform {0}; //T_in\n'.format(T_in)
replaceIn(file, 'T_in', stringToInsert)

# processor number
file = 'system/decomposeParDict'
stringToInsert = 'numberOfSubdomains {0};\n'.format(n_proc)
replaceIn(file, 'numberOfSubdomains', stringToInsert)
file = 'slurm-job.sh'
stringToInsert = '#SBATCH --ntasks={0}               # Number of tasks\n'.format(n_proc)
replaceIn(file, 'ntasks', stringToInsert)
stringToInsert = 'mpirun -np {0} buoyantSimpleFoam -parallel >./log-run\n'.format(n_proc)
replaceIn(file, 'buoyantSimpleFoam', stringToInsert)
file = 'calculate-C/system/decomposeParDict'
stringToInsert = 'numberOfSubdomains {0};\n'.format(n_proc)
replaceIn(file, 'numberOfSubdomains', stringToInsert)
file = 'calculate-C/slurm-job-C.sh'
stringToInsert = '#SBATCH --ntasks={0}               # Number of tasks\n'.format(n_proc)
replaceIn(file, 'ntasks', stringToInsert)
stringToInsert = 'mpirun -np {0} scalarTransportTurbulentFoam -limit -parallel >./log-run\n'.format(n_proc)
replaceIn(file, 'scalarTransportTurbulentFoam', stringToInsert)