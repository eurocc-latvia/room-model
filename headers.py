import os
from shutil import copyfile
def replaceIn(fileName, patternToDelete, stringToInsert):
    fileNameOut = fileName+'-out'
    fIn = open(fileName) # 'r' argument by default
    fOut = open(fileNameOut,'w')
    while True:
        line = fIn.readline()
        if len(line)==0:
            break
        if (patternToDelete in line):
            print('  Replacing a line in the file {0}...'.format(fileName))
            fOut.write(stringToInsert)
        else:
            fOut.write(line)
    fIn.close()
    fOut.close()
    os.remove(fileName)
    copyfile(fileNameOut, fileName)
    os.remove(fileNameOut)

blockMeshDictHeader = """
/*--------------------------------*- C++ -*----------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Version:  6
     \\/     M anipulation  |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      blockMeshDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

convertToMeters 1;

vertices
(
"""

blockMeshDictFooter = """
    )
);

edges
(
);

boundary
(
    fixedWalls
    {
        type wall;
        faces
        (
			(3 7 6 2)
            (0 4 7 3)
            (2 6 5 1)
            (1 5 4 0)
            (0 3 2 1)
            (4 5 6 7)
        );
    }
);

mergePatchPairs
(
);

// ************************************************************************* //
"""