import matplotlib.pyplot as plt
import matplotlib.tri as tri
from matplotlib.patches import Rectangle
import numpy as np
import pandas as pd
import os
from shapely.geometry import Polygon, Point
import alphashape

def importData(casepath):

    #  Reading velocity data
    df_U = pd.read_csv(
            casepath + "/U_vertical_plane_2.raw",
            sep = " ",
            comment = "#",
            names = ["x", "y", "z", "U_x", "U_y", "U_z"]
            )
            
    # Reading temperature data
    df_T = pd.read_csv(
            casepath + "/T_vertical_plane_2.raw",
            sep = " ",
            comment = "#",
            names = ["x", "y", "z", "T"]
            )
            
    # Joining both dataframes into one
    df = pd.merge(df_U,df_T, how="inner")
    
    # Data processing (velocity magnitude)
    df["U_avg"] = np.sqrt(df["U_x"]**2+df["U_y"]**2+df["U_z"]**2)
    
    return df
               

alpha = 8 # decrease to include more points
def plotData(dataframe):
    
    # Interpolation grid size
    n_grid_x = 200
    n_grid_y = 30
    
    # Converting to Numpy arrays for more stable operation
    x = dataframe["y"].to_numpy()
    y = dataframe["z"].to_numpy()
    T = dataframe["T"].to_numpy()
    U = dataframe["U_avg"].to_numpy()
    
    # Creating interpolation functions for parameters
    xi = np.linspace(x.min(), x.max(), n_grid_x)
    yi = np.linspace(y.min(), y.max(), n_grid_y)
    triang = tri.Triangulation(x, y)
    interpolator_T = tri.LinearTriInterpolator(triang, T)
    interpolator_U = tri.LinearTriInterpolator(triang, U)
    
    # Preparing arrays for plotting (using interpolation functions)
    Xi, Yi = np.meshgrid(xi, yi)
    Ti = interpolator_T(Xi, Yi)
    Ui = interpolator_U(Xi, Yi)
    
    # Removing points that are outside the simulation region
    mpoints = [(X, Y) for X, Y in zip(x, y)]
    # alpha = 0.97 * alphashape.optimizealpha(mpoints, lower=0.1, upper=100)
    # print('Optimized alpha = ', alpha)
    hull = alphashape.alphashape(mpoints, alpha)
    poly = Polygon(hull)
    for i in range(len(Xi)):
        for j in range(len(Xi[0])):
            if not np.isnan(Ti[i,j]): # no need to test pts that are already NaN
                p1 = Point(Xi[i,j], Yi[i,j])
                test = poly.contains(p1) # | poly.touches(p1)
                if test==False:
                    Ti[i,j]=np.nan
                    Ui[i,j]=np.nan
    
    # Defining subplots
    fig, (axis_top, axis_bot) = plt.subplots(
        nrows = 2,
        ncols = 1,
        figsize = (5,6),
        dpi = 150
    )
    
    # Creating space between subplots
    plt.subplots_adjust(hspace = 0.3)
    #plt.style.use('dark_background')
    
    # Setting common parameters for both subplots
    for ax in (axis_top, axis_bot):
        ax.set_aspect(1.0)
        ax.set_xlim((x.min(),x.max()))
        ax.set_ylim((y.min(),y.max()))
        ax.set_xlabel("$y$, $\mathrm{m}$")
        ax.set_ylabel("$z$, $\mathrm{m}$")
        # ax.add_patch(
            # Rectangle(
                # (0.6, 0.0),
                # 0.7,
                # 0.5,
                # fc ='gray',
                # ec ='black',
                # lw = 0.5
                # )
            # )
    
    # Plotting TEMPERATURE
    axis_top.set_title('Temperature')
    templevels = np.linspace(284-273, 296-273, 21)
    contours = axis_top.contourf(xi, yi, Ti-273.0, levels=templevels, cmap="coolwarm")
    fig.colorbar(
        contours,
        ax = axis_top,
        label = "$T$, $\mathrm{°C}$"
        )
    
    # Plotting VELOCITY
    axis_bot.set_title('Velocity magnitude')
    vellevels = np.linspace(0, 0.6, 11)
    contours = axis_bot.contourf(xi, yi, Ui, levels=vellevels, cmap="viridis")
    fig.colorbar(
        contours,
        ax = axis_bot,
        label = "$|\\vec{U}|$, $\mathrm{\\frac{m}{s}}$"
        )
    
    return fig


# for alpha in [10, 10.5, 11, 11.5]:
    # print(alpha, '...')
resultsDir = 'postProcessing/planeAnalysis/'
for timeDir in os.listdir(resultsDir):
    dataframe = importData(resultsDir+timeDir)
    figure = plotData(dataframe)
    figure.savefig("surface-T-and-U-YZ-{0}s.png".format(timeDir))



