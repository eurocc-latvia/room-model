import matplotlib.pyplot as plt
import numpy as np

probes_door = np.loadtxt('postProcessing/probes_door/0/T')
probes_window = np.loadtxt('postProcessing/probes_window/0/T')

plt.figure(figsize=(10,5))
plt.plot(probes_door[:,0], probes_door[:,1], label='door')
plt.plot(probes_window[:,0], probes_window[:,1], label='window')
ax = plt.gca()
ax.set_xlabel('t, s')
ax.set_ylabel('T, K')
plt.legend()
plt.savefig('probes-T.png')

probes_door = np.genfromtxt('postProcessing/probes_door/0/U', deletechars=")")
probes_window = np.genfromtxt('postProcessing/probes_window/0/U', deletechars=")")

plt.figure(figsize=(10,5))
plt.plot(probes_door[:,0], probes_door[:,2], label='door')
plt.plot(probes_window[:,0], probes_window[:,2], label='window')
ax = plt.gca()
ax.set_xlabel('t, s')
ax.set_ylabel('v_y, m/s')
plt.legend()
plt.savefig('probes-vy.png')