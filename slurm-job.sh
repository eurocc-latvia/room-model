#!/bin/bash
#SBATCH --job-name=EuroCC-demo_air-flow  # Job name
#SBATCH --ntasks=16                      # Number of tasks
#SBATCH --partition=regular              # Partition name
#SBATCH --mem-per-cpu=2G                 # Memory per cpu-core
#SBATCH --time=694:00:00                 # Time limit hrs:min:sec

echo "Current dir = "; pwd; hostname; date
decomposePar -force
mpirun -np 16 buoyantSimpleFoam -parallel >./log-run
echo ""; echo ""; echo ""; echo "" 
echo "Simulations are completed! Current time:"
date

echo ""; echo ""; echo ""; echo "" 
echo "Drawing results:"
./4-draw-results.sh
echo ""; echo ""; echo ""; echo "" 
echo "Simulations and visualisation are completed! Current time:"
date
