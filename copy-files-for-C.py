import os
from shutil import copyfile, copytree, rmtree

# find the latest time
timeDirMax = '0'
for timeDir in os.listdir():
    try:
        time = float(timeDir)
        if time>float(timeDirMax):
            timeDirMax = timeDir
    except ValueError:
        pass

print('Copying velocity and turbulent viscosity fields from '+timeDirMax+' directory...')
copyfile(timeDirMax+'/U', 'calculate-C/0/U')
copyfile(timeDirMax+'/nut', 'calculate-C/0/nut')

try:
    copytree('constant/polyMesh', 'calculate-C/constant/polyMesh')
except FileExistsError:
    rmtree('calculate-C/constant/polyMesh')
    copytree('constant/polyMesh', 'calculate-C/constant/polyMesh')