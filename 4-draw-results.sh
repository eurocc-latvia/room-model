#!/bin/bash
### convergence of T and U during the calculations
python plot-probes.py 

### Velocity vectors in three different slices
reconstructPar -latestTime
pvpython draw-v.py

### Air temperature in three different slices
pvpython draw-T.py

### T over the line, defined in system/singleGraph file
postProcess -latestTime -func singleGraph
python plot-line.py 

### v and U in the slice, defined in system/planeAnalysis file
postProcess -func planeAnalysis -latestTime > log-postProcess
# python plot-surface.py 
# python plot-surface-2.py 

### VTK folder will be created, with results that can be viewed with ParaView
foamToVTK -latestTime
