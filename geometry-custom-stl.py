import openpyscad as ops
import numpy as np
import stl
from stl import mesh
from parameters import *
from headers import *
eps = 0.001 # offset/precision for geometry creation

def find_mins_maxs(obj):
    minx = maxx = miny = maxy = minz = maxz = None
    for p in obj.points:
        # p contains (x, y, z)
        if minx is None:
            minx = p[stl.Dimension.X]
            maxx = p[stl.Dimension.X]
            miny = p[stl.Dimension.Y]
            maxy = p[stl.Dimension.Y]
            minz = p[stl.Dimension.Z]
            maxz = p[stl.Dimension.Z]
        else:
            maxx = max(p[stl.Dimension.X], maxx)
            minx = min(p[stl.Dimension.X], minx)
            maxy = max(p[stl.Dimension.Y], maxy)
            miny = min(p[stl.Dimension.Y], miny)
            maxz = max(p[stl.Dimension.Z], maxz)
            minz = min(p[stl.Dimension.Z], minz)
    return minx, maxx, miny, maxy, minz, maxz

arrayOfSurfaces = mesh.Mesh.from_multi_file('constant/triSurface/custom-geometry.stl')
mins_maxs = []
file = 'system/snappyHexMeshDictCustom'
print("Analyzing constant/triSurface/custom-geometry.stl...")
for ii in arrayOfSurfaces:
    print("  Found surface called ", ii.name.decode("utf-8"), " with dimensions ", find_mins_maxs(ii))
    stringToInsert = '            //List of surfaces:\n            {0}           {{name {0}          ;}}\n'.format(ii.name.decode("utf-8"))
    replaceIn(file, '//List of surfaces', stringToInsert)
    if ii.name == b'in':
        print("    Detecting inlet position for mesh refinement...")
        dims = find_mins_maxs(ii)
        inletMidX = (dims[0] + dims[1])/2
        inletMidY = (dims[2] + dims[3])/2
        inletMidZ = (dims[4] + dims[5])/2
        inletSize = max(dims[1] - dims[0], dims[3] - dims[2], dims[5] - dims[4])
        print("    Detected ", inletMidX, inletMidY, inletMidZ)
    if ii.name == b'walls':
        volume, cog, inertia = ii.get_mass_properties()
        print("    Detected room center: ", *cog)
    mins_maxs.append(find_mins_maxs(ii))
mins_maxs = np.array(mins_maxs)
bounding_box = [] # contains these numbers: minx, maxx, miny, maxy, minz, maxz
bounding_box.append(min(mins_maxs[:,0]))
bounding_box.append(max(mins_maxs[:,1]))
bounding_box.append(min(mins_maxs[:,2]))
bounding_box.append(max(mins_maxs[:,3]))
bounding_box.append(min(mins_maxs[:,4]))
bounding_box.append(max(mins_maxs[:,5]))
# print(bounding_box)
x_room = bounding_box[1]-bounding_box[0]
y_room = bounding_box[3]-bounding_box[2]
z_room = bounding_box[5]-bounding_box[4]

print('Moving a human figure and writing instructions for OpenSCAD...')
stringToInsert = 'translate([{0},{1},0-0.01]) \n'.format(x_man_postion, y_man_postion)
replaceIn('geometry-man.scad', 'translate(', stringToInsert)

# Changing blockmeshDict: size of outer boundaries, number of cells, mesh layers
print('Changing blockmeshDict...')
fOut = open('system/blockMeshDict', 'w')
fOut.write(blockMeshDictHeader)
fOut.write('\t({0}   {1}   {2})\n'.format(bounding_box[0]-eps, bounding_box[2]-eps, bounding_box[4]-eps))
fOut.write('\t({0}   {1}   {2})\n'.format(bounding_box[1]+eps, bounding_box[2]-eps, bounding_box[4]-eps))
fOut.write('\t({0}   {1}   {2})\n'.format(bounding_box[1]+eps, bounding_box[3]+eps, bounding_box[4]-eps))
fOut.write('\t({0}   {1}   {2})\n'.format(bounding_box[0]-eps, bounding_box[3]+eps, bounding_box[4]-eps))
fOut.write('\t({0}   {1}   {2})\n'.format(bounding_box[0]-eps, bounding_box[2]-eps, bounding_box[5]+eps))
fOut.write('\t({0}   {1}   {2})\n'.format(bounding_box[1]+eps, bounding_box[2]-eps, bounding_box[5]+eps))
fOut.write('\t({0}   {1}   {2})\n'.format(bounding_box[1]+eps, bounding_box[3]+eps, bounding_box[5]+eps))
fOut.write('\t({0}   {1}   {2})\n'.format(bounding_box[0]-eps, bounding_box[3]+eps, bounding_box[5]+eps))
fOut.write(""");

blocks
(
    hex (0 1 2 3 4 5 6 7) """)
fOut.write('\t({0}   {1}   {2})\n'.format(int(n_x*x_room), int(n_y*y_room), int(n_z*z_room)))
fOut.write("""    simpleGrading 
    (
        (""")
cellsNearWallX = n_x*layer*(1+rate/5)
cellsNearWallY = n_y*layer*(1+rate/5)
cellsNearWallZ = n_z*layer*(1+rate/5)
fOut.write('\t\t\t({0}  {1}  {2})\n'.format(layer, cellsNearWallX, 1))
fOut.write('\t\t\t({0}  {1}  {2})\n'.format((x_room/2-layer), n_x*x_room/2-cellsNearWallX, rate))
fOut.write('\t\t\t({0}  {1}  {2})\n'.format((x_room/2-layer), n_x*x_room/2-cellsNearWallX, 1/rate))
fOut.write('\t\t\t({0}  {1}  {2})\n'.format(layer, cellsNearWallX, 1))
fOut.write('\t\t) // x-direction\n')
fOut.write('\t\t(\n')
fOut.write('\t\t\t({0}  {1}  {2})\n'.format(layer, cellsNearWallY, 1))
fOut.write('\t\t\t({0}  {1}  {2})\n'.format((y_room/2-layer), n_y*y_room/2-cellsNearWallY, rate))
fOut.write('\t\t\t({0}  {1}  {2})\n'.format((y_room/2-layer), n_y*y_room/2-cellsNearWallY, 1/rate))
fOut.write('\t\t\t({0}  {1}  {2})\n'.format(layer, cellsNearWallY, 1))
fOut.write('\t\t) // y-direction\n')
fOut.write('\t\t(\n')
fOut.write('\t\t\t({0}  {1}  {2})\n'.format(layer, cellsNearWallZ, 1))
fOut.write('\t\t\t({0}  {1}  {2})\n'.format((z_room/2-layer), n_z*z_room/2-cellsNearWallZ, rate))
fOut.write('\t\t\t({0}  {1}  {2})\n'.format((z_room/2-layer), n_z*z_room/2-cellsNearWallZ, 1/rate))
fOut.write('\t\t\t({0}  {1}  {2})\n'.format(layer, cellsNearWallZ, 1))
fOut.write('\t\t) // z-direction\n')
fOut.write(blockMeshDictFooter)
fOut.close()


# Writing coordinates of patches in system/topoSetDict_*
# print('Writing coordinates of patches...')
# eps2 = eps/3 # for more precise selection of surfaces
### ceiling
# stringToInsert = '            box ({0} {1} {2}) ({3} {4} {5});\n'.format(-x_door-eps2, -eps2, z_room-eps2,   x_room+x_win+eps2, y_room+eps2, z_room+eps2)
# replaceIn('system/topoSetDict_ceiling', 'box (', stringToInsert)
### floor
# stringToInsert = '            box ({0} {1} {2}) ({3} {4} {5});\n'.format(-x_door-eps2, -eps2, -eps,   x_room+x_win+eps2, y_room+eps2, eps)
# replaceIn('system/topoSetDict_floor', 'box (', stringToInsert)
### door
# stringToInsert = '            box ({0} {1} {2}) ({3} {4} {5});\n'.format(-x_door-eps2, y_door_position-eps2, -eps2,   -x_door+eps2, y_door_position+y_door+eps2, z_door+eps2)
# replaceIn('system/topoSetDict_door', 'box (', stringToInsert)
### window
# stringToInsert = '            box ({0} {1} {2}) ({3} {4} {5});\n'.format(x_room+x_door-eps2, y_win_position-eps2, z_win_position-eps2,   x_room+x_door+eps2, y_win_position+y_win+eps2, z_win_position+z_win+eps2)
# replaceIn('system/topoSetDict_window', 'box (', stringToInsert)
### in
# stringToInsert = '            box ({0} {1} {2}) ({3} {4} {5});\n'.format(condHoriz-eps2, y_cond_position+eps2, z_cond_position-eps2,   x_cond+eps2, y_cond_position+y_cond-eps2, z_cond_position+(x_cond-condHoriz)+eps2)
# replaceIn('system/topoSetDict_in', 'box (', stringToInsert)
### out
# stringToInsert = '		    p1          ({0} {1} {2});\n'.format(x_room-eps2, y_outlet_position, z_outlet_position)
# replaceIn('system/topoSetDict_out', 'p1 ', stringToInsert)
# stringToInsert = '		    p2          ({0} {1} {2});\n'.format(x_room+eps2, y_outlet_position, z_outlet_position)
# replaceIn('system/topoSetDict_out', 'p2 ', stringToInsert)
# stringToInsert = '            radius      {0};\n'.format(r_outlet)
# replaceIn('system/topoSetDict_out', 'radius ', stringToInsert)

# nose of a human
x_nose = x_man_postion - 0.105
z_nose = 1.4235
stringToInsert = '		    p1          ({0} {1} {2});\n'.format(x_nose, y_man_postion-0.1, z_nose)
replaceIn('system/topoSetDict_nose', 'p1 ', stringToInsert)
stringToInsert = '		    p2          ({0} {1} {2});\n'.format(x_nose, y_man_postion, z_nose)
replaceIn('system/topoSetDict_nose', 'p2 ', stringToInsert)



# changing regions that will be refined in system/snappyHexMeshDictCustom
print('Modifying refinement regions in snappyHexMeshDictCustom...')
file = 'system/snappyHexMeshDictCustom'
# large cylinder
stringToInsert = '		point1 ({0} {1} {2}); //p1-c1\n'.format(inletMidX, inletMidY-inletSize*1.3, inletMidZ)
replaceIn(file, 'p1-c1', stringToInsert)
stringToInsert = '		point2 ({0} {1} {2}); //p2-c1\n'.format(inletMidX, inletMidY+inletSize*1.3, inletMidZ)
replaceIn(file, 'p2-c1', stringToInsert)
stringToInsert = '		radius {0}; //r-c1\n'.format(inletSize*1.3)
replaceIn(file, 'r-c1', stringToInsert)
# smaller cylinder
stringToInsert = '		point1 ({0} {1} {2}); //p1-c2\n'.format(inletMidX, inletMidY-inletSize/1.3, inletMidZ)
replaceIn(file, 'p1-c2', stringToInsert)
stringToInsert = '		point2 ({0} {1} {2}); //p2-c2\n'.format(inletMidX, inletMidY+inletSize/1.3, inletMidZ)
replaceIn(file, 'p2-c2', stringToInsert)
stringToInsert = '		radius {0}; //r-c2\n'.format(inletSize/1.3)
replaceIn(file, 'r-c2', stringToInsert)
# cylinder around the human's nose
stringToInsert = '		point1 ({0} {1} {2}); //p1-c3\n'.format(x_nose, y_man_postion-0.2, z_nose)
replaceIn(file, 'p1-c3', stringToInsert)
stringToInsert = '		point2 ({0} {1} {2}); //p2-c3\n'.format(x_nose, y_man_postion, z_nose)
replaceIn(file, 'p2-c3', stringToInsert)
stringToInsert = '		radius {0}; //r-c3\n'.format(r_cyl_man)
replaceIn(file, 'r-c3', stringToInsert)
# define a point inside the mesh
stringToInsert = '    locationInMesh ({0} {1} {2});\n'.format(*cog)
replaceIn(file, 'locationInMesh (', stringToInsert)



# modifying the file that gives instructions for mesh visualization
file = 'draw-mesh.py'
# YZ plane
stringToInsert = 'clip1.ClipType.Origin = [{0}+0.001, {1}, {2}] # originYZ\n'.format(x_man_postion, y_man_postion, bounding_box[4]+z_room/2)
replaceIn(file, 'originYZ', stringToInsert)
stringToInsert = 'renderView1.CameraPosition = [{0}, {1}, {2}] # camposYZ\n'.format(bounding_box[2]-y_room*4, bounding_box[2]+y_room/2, bounding_box[4]+z_room/2)
replaceIn(file, 'camposYZ', stringToInsert)
stringToInsert = 'renderView1.CameraFocalPoint = [{0}, {1}, {2}] # camfocYZ\n'.format(bounding_box[2]+y_room/4, bounding_box[2]+y_room/2, bounding_box[4]+z_room/2)
replaceIn(file, 'camfocYZ', stringToInsert)
# XZ plane
stringToInsert = 'clip1.ClipType.Origin = [{0}, {1}+0.001, {2}] # originXZ\n'.format(x_man_postion, y_man_postion, bounding_box[4]+z_room/2)
replaceIn(file, 'originXZ', stringToInsert)
stringToInsert = 'renderView1.CameraPosition = [{0}, {1}, {2}] # camposXZ\n'.format(bounding_box[0]+x_room/2, bounding_box[0]-x_room*4, bounding_box[4]+z_room/2)
replaceIn(file, 'camposXZ', stringToInsert)
stringToInsert = 'renderView1.CameraFocalPoint = [{0}, {1}, {2}] # camfocXZ\n'.format(bounding_box[0]+x_room/2, bounding_box[0]+x_room/4, bounding_box[4]+z_room/2)
replaceIn(file, 'camfocXZ', stringToInsert)
# XY plane
stringToInsert = 'clip1.ClipType.Origin = [{0}, {1}, {2}+0.001] # originXY\n'.format(x_man_postion, y_man_postion, bounding_box[4]+z_room/2)
replaceIn(file, 'originXY', stringToInsert)
stringToInsert = 'renderView1.CameraPosition = [{0}, {1}, {2}] # camposXY\n'.format(bounding_box[0]+x_room/2, bounding_box[2]+y_room/2, bounding_box[4]+z_room*5.2)
replaceIn(file, 'camposXY', stringToInsert)
stringToInsert = 'renderView1.CameraFocalPoint = [{0}, {1}, {2}] # camfocXY\n'.format(bounding_box[0]+x_room/2, bounding_box[2]+y_room/2, bounding_box[4]+3*z_room/4)
replaceIn(file, 'camfocXY', stringToInsert)


# modifying the file that gives instructions for velocity visualization
file = 'draw-v.py'
# YZ plane
stringToInsert = 'slice1.SliceType.Origin = [{0}+0.001, {1}, {2}] # originYZ\n'.format(x_man_postion, y_man_postion, bounding_box[4]+z_room/2)
replaceIn(file, 'originYZ', stringToInsert)
stringToInsert = 'renderView1.CameraPosition = [{0}, {1}, {2}] # camposYZ\n'.format(bounding_box[2]-y_room*4, bounding_box[2]+y_room/2, bounding_box[4]+z_room/2)
replaceIn(file, 'camposYZ', stringToInsert)
stringToInsert = 'renderView1.CameraFocalPoint = [{0}, {1}, {2}] # camfocYZ\n'.format(bounding_box[2]+y_room/4, bounding_box[2]+y_room/2, bounding_box[4]+z_room/2)
replaceIn(file, 'camfocYZ', stringToInsert)
# XZ plane
stringToInsert = 'slice1.SliceType.Origin = [{0}, {1}+0.001, {2}] # originXZ\n'.format(x_man_postion, y_man_postion, bounding_box[4]+z_room/2)
replaceIn(file, 'originXZ', stringToInsert)
stringToInsert = 'renderView1.CameraPosition = [{0}, {1}, {2}] # camposXZ\n'.format(bounding_box[0]+x_room/2, bounding_box[0]-x_room*4, bounding_box[4]+z_room/2)
replaceIn(file, 'camposXZ', stringToInsert)
stringToInsert = 'renderView1.CameraFocalPoint = [{0}, {1}, {2}] # camfocXZ\n'.format(bounding_box[0]+x_room/2, bounding_box[0]+x_room/4, bounding_box[4]+z_room/2)
replaceIn(file, 'camfocXZ', stringToInsert)
# XY plane
stringToInsert = 'slice1.SliceType.Origin = [{0}, {1}, {2}+0.001] # originXY\n'.format(x_man_postion, y_man_postion, bounding_box[4]+z_room/2)
replaceIn(file, 'originXY', stringToInsert)
stringToInsert = 'renderView1.CameraPosition = [{0}, {1}, {2}] # camposXY\n'.format(bounding_box[0]+x_room/2, bounding_box[2]+y_room/2, bounding_box[4]+z_room*5.2)
replaceIn(file, 'camposXY', stringToInsert)
stringToInsert = 'renderView1.CameraFocalPoint = [{0}, {1}, {2}] # camfocXY\n'.format(bounding_box[0]+x_room/2, bounding_box[2]+y_room/2, bounding_box[4]+3*z_room/4)
replaceIn(file, 'camfocXY', stringToInsert)


# modifying the file that gives instructions for temperature visualization
file = 'draw-T.py'
# YZ plane
stringToInsert = 'slice1.SliceType.Origin = [{0}+0.001, {1}, {2}] # originYZ\n'.format(x_man_postion, y_man_postion, bounding_box[4]+z_room/2)
replaceIn(file, 'originYZ', stringToInsert)
stringToInsert = 'renderView1.CameraPosition = [{0}, {1}, {2}] # camposYZ\n'.format(bounding_box[2]-y_room*4, bounding_box[2]+y_room/2, bounding_box[4]+z_room/2)
replaceIn(file, 'camposYZ', stringToInsert)
stringToInsert = 'renderView1.CameraFocalPoint = [{0}, {1}, {2}] # camfocYZ\n'.format(bounding_box[2]+y_room/4, bounding_box[2]+y_room/2, bounding_box[4]+z_room/2)
replaceIn(file, 'camfocYZ', stringToInsert)
# XZ plane
stringToInsert = 'slice1.SliceType.Origin = [{0}, {1}+0.001, {2}] # originXZ\n'.format(x_man_postion, y_man_postion, bounding_box[4]+z_room/2)
replaceIn(file, 'originXZ', stringToInsert)
stringToInsert = 'renderView1.CameraPosition = [{0}, {1}, {2}] # camposXZ\n'.format(bounding_box[0]+x_room/2, bounding_box[0]-x_room*4, bounding_box[4]+z_room/2)
replaceIn(file, 'camposXZ', stringToInsert)
stringToInsert = 'renderView1.CameraFocalPoint = [{0}, {1}, {2}] # camfocXZ\n'.format(bounding_box[0]+x_room/2, bounding_box[0]+x_room/4, bounding_box[4]+z_room/2)
replaceIn(file, 'camfocXZ', stringToInsert)
# XY plane
stringToInsert = 'slice1.SliceType.Origin = [{0}, {1}, {2}+0.001] # originXY\n'.format(x_man_postion, y_man_postion, bounding_box[4]+z_room/2)
replaceIn(file, 'originXY', stringToInsert)
stringToInsert = 'renderView1.CameraPosition = [{0}, {1}, {2}] # camposXY\n'.format(bounding_box[0]+x_room/2, bounding_box[2]+y_room/2, bounding_box[4]+z_room*5.2)
replaceIn(file, 'camposXY', stringToInsert)
stringToInsert = 'renderView1.CameraFocalPoint = [{0}, {1}, {2}] # camfocXY\n'.format(bounding_box[0]+x_room/2, bounding_box[2]+y_room/2, bounding_box[4]+3*z_room/4)
replaceIn(file, 'camfocXY', stringToInsert)


# modifying the file that gives instructions for velocity visualization
file = 'calculate-C/draw-C.py'
# XZ plane
stringToInsert = 'slice1.SliceType.Origin = [{0}, {1}+0.001, {2}] # originXZ\n'.format(x_man_postion, y_man_postion, bounding_box[4]+z_room/2)
replaceIn(file, 'originXZ', stringToInsert)
stringToInsert = 'renderView1.CameraPosition = [{0}, {1}, {2}] # camposXZ\n'.format(bounding_box[0]+x_room/2, bounding_box[0]-x_room*4, bounding_box[4]+z_room/2)
replaceIn(file, 'camposXZ', stringToInsert)
stringToInsert = 'renderView1.CameraFocalPoint = [{0}, {1}, {2}] # camfocXZ\n'.format(bounding_box[0]+x_room/2, bounding_box[0]+x_room/4, bounding_box[4]+z_room/2)
replaceIn(file, 'camfocXZ', stringToInsert)
